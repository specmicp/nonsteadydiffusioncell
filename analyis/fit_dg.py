#Copyright (c) 2018, 2019 Fabien Georget <fabien.georget@epfl.ch>, EPFL
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""Module to analyse O2 diffusion cell results.

Usage:
    >>> geometry = get_default_geometry()
    >>> # change geometry if needed
    >>> fit_experiment(<filename.csv>,geometry,calib=None,verbose=True)

<filename.csv> is a csv file with the  experimental data:
     - 2 columns separated by comma or semi-colon
     - 1st: time (h), 2nd: 02 in N2 content (%)
     - 1 line of header

If calib (scalar) is provided, the data is normalized with respect to that value.
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import scipy.signal as ssi

def get_default_geometry():
    """Return the default EPFL-LMC cell geometry"""
    data = {
        "thickness_sample": 0.1,  # cm
        "length_chamber": 8.167, # cm
        "c_ext": 21.0,       # ambient O2 in N2
        "diameter_sample": 3.0,#cm
        "diameter_chamber": 3.0 #cm
    }

    return data

def get_lafarge_geometry():
    """Return the default LCR (LafargeHolcim) geometry"""
    data = {
        "thickness_sample": 0.1,  # cm
        "length_chamber": 9.0, # cm
        "c_ext": 21.0,       # ambient O2 in N2
        "diameter_sample": 3.0,#cm
        "diameter_chamber": 4.0 #cm
    }

    return data


def get_new_setup_geometry():
    """Return the default EPFL-LMC cell geometry"""
    return get_default_geometry()

def get_geometry(setup, thickness):
    """Return a geometry by name

    :param setup: name of the setup
    :param thickness: thickness of the sample
    """
    if setup=="new_epfl_setup":
        data=dict(get_new_setup_geometry())
        data["thickness_sample"]=thickness
        return data
    elif setup=="lafarge_setup":
        data=dict(get_lafarge_geometry())
        data["thickness_sample"]=thickness
        return data
    else:
        raise ValueError("Unknown setup: {0}".format(setup))


def get_func_diffusion(c0, geometry):
    """Analytical solution to the diffusion equation through the sample"""
    dx_s_dx_c = geometry["thickness_sample"]*geometry["length_chamber"]
    ratio_d_square = (geometry["diameter_sample"]/geometry["diameter_chamber"])**2
    c_ext = geometry["c_ext"]

    def diffusion(xts,diffusion_sample):
        ys=[c0]
        for i in range(1, len(xts)):
            dt = xts[i]-xts[i-1]
            factor = dt*float(diffusion_sample)*ratio_d_square/(dx_s_dx_c)
            y = 1.0/float((1.0+factor))*(ys[i-1]+factor*c_ext)
            ys.append(y)
        return ys
    return diffusion

def extract_from_csv(filename, c_ext, calib=None, remove_bad_data=True):
    """Obtain the experimental data from the CSV.

    :param filename: csv file
    :param c_ext: O2 in air
    :param calib: (float) O2 in air as read by the sensor
    :param remove_bad_data: if true, filter bad data (nan, and anomalous peaks)
    """
    try:
        exp_data = np.genfromtxt(filename, delimiter=",", skip_header=1, usecols=(0,1))
    except ValueError:
        exp_data = np.genfromtxt(filename, delimiter=";", skip_header=1, usecols=(0,1))

    if remove_bad_data:
        exp_data = exp_data[~np.isnan(exp_data[:,1]),:]
        peaks, other = ssi.find_peaks(exp_data[:,1], 1, distance=100, prominence=0.1)
        w, inter, ipsl, ipsr = ssi.peak_widths(exp_data[:,1], peaks, rel_height=1)
        index_to_removes = []
        for ind,peak in enumerate(peaks):
            index_to_removes.extend(np.arange(max(int(peak-w[ind]),0), min(int(peak+w[ind]),len(exp_data)-1),1))
        exp_data=exp_data[[i for i in range(len(exp_data[:,0])) if i not in index_to_removes],:]

    exp_data[:,0] -= exp_data[0, 0] # start at t = 0
    exp_data[:,0] *= 3600       # transform time unit : h -> s

    if calib is None:
        calib = exp_data[-1, 1]

    exp_data[:,1] *= c_ext/calib # normalize data

    if exp_data[0,0] != 0.0:
        exp_data[:,0] -= exp_data[0,0]

    return exp_data


def fit_experiment(filename,geometry,calib=None,verbose=True,remove_bad_data=True):
    """Back calculate the diffusion coefficient from an experiment given a
    geometry.

    :param filename: csv file containing the data
    :param geometry: geometry of the cell and the sample
    :param calib: O2 in N2 as read by the sensor (if None, read from the last value)
    :param verbose: if True, plot the experimetnal datapoints and the fit
    :param remove_bad_data: if True, remove NaN and anomalous peaks in data

    """
    exp_data = extract_from_csv(filename, geometry["c_ext"], calib, remove_bad_data=remove_bad_data)
    xts = exp_data[:,0]
    ys = exp_data[:,1]

    func = get_func_diffusion(ys[0],geometry)
    popt, pcov = sco.curve_fit(func,xts,ys,p0=1e-4)

    De = popt[0]
    perr = np.sqrt(pcov[0])[0]
    if verbose:
        print("Analysis for {0}: \n - De = {1:.3e} cm^2/s\n - perr = {2:.3e}".format(filename, popt[0], perr))

    fitted_ys = func(xts, popt[0])

    if verbose:
        plt.plot(xts/3600, ys, ".", label=filename)
        plt.plot(xts/3600, fitted_ys, "-", label="fit")
        plt.xlabel("time (h)")
        plt.ylabel("O2 in N2 (%)")
        plt.legend()

    return ((De, perr), (xts/3600, ys, fitted_ys))