# non-steady state diffusion cell 

A diffusion cell to measure the effective gas diffusion coefficient of thin disk.


## Content

analysis/ : fit experimental results
setup/ : drawings of the diffusion chamber
doc/ :

## Citations

Changes of microstructure and diffusivity in blended cement pastes exposed to natural carbonation
Wioletta Soja, Hamed Maraghechi, Fabien Georget and Karen Scrivener
MATEC Web Conf., 199 (2018) 02009
DOI: https://doi.org/10.1051/matecconf/201819902009

Original work:

A new test method to determine the gaseous oxygen diffusion coefficient of cement pastes as a function of hydration duration, microstructure, and relative humidity
Boumaaza, M., Huet, B., Pham, G. et al.
Mater Struct (2018) 51: 51.
https://doi.org/10.1617/s11527-018-1178-z
